#include "csapp.h"

int main(int argc, char **argv) {
	int clientfd;
	char *port;
	char *host, buf[MAXLINE];
	rio_t rio;
	// ----------------------
	int i = 0, lenw = 256;
	int fsize;
	char command[256];

	// ----------------------

	if (argc != 3) {
		fprintf(stderr, "usage: %s <host> <port>\n", argv[0]);
		exit(0);
	}
	host = argv[1];
	port = argv[2];

	clientfd = Open_clientfd(host, port);
	Rio_readinitb(&rio, clientfd);

//	while (Fgets(buf, MAXLINE, stdin) != NULL) {
	while (1) {

		//strcpy(command, "ls -la");
        printf("Ingrese un comando: ");
        fgets(command,256,stdin);
        
        send((int)clientfd, command, sizeof(command), 0);

		Rio_writen(clientfd, buf, strlen(buf));
		Rio_readn((int)clientfd, buf, lenw);

        if ( (int)buf == -1 ) {
            printf("\n > No se pudo realizar la trasferencia.\n > Cerrando conexion.\n");
            Close(clientfd);
            exit(0);
        }
        else {
            if ( i == -2 ) {
                printf("\n------------------------------------");
                printf("\n > Tamaño de respuesta: %s bytes.\n", buf );
//                len = (int) buf;
                fsize = atoi(buf);
                i++;                
            }
            else {
//                buf[fsize] = '\0';
                printf(" > %s\n", buf );
/*
                printf("\n > Escribiendo archivo...");
                received_file = fopen(FILENAME, "w");
                if (received_file == NULL) {
                    fprintf(stderr, " > Error al escribir el archivo: %s\n", strerror(errno));
                    Close(clientfd);
                    exit(0);
                }
                // Escribimos el archivo 
                fwrite(buf, 1, sizeof(buf), received_file);
                fclose(received_file);
*/
                Close(clientfd);
                printf("\n > Conexion terminada.\n------------------------------------\n");
                exit(0);
            }

        }
	}
	Close(clientfd);
	exit(0);
}
