#include "csapp.h"

void echo(int connfd);

int main(int argc, char **argv){
	int listenfd, connfd;
	unsigned int clientlen;
	struct sockaddr_in clientaddr;
	struct hostent *hp;
	char *haddrp, *port;
	// --------------------------
	char response[256];
	char buf[MAXLINE];
	char *res = "1. Respuesta al comando enviado\n2. Esta es otra linea.";
	int check;



	// --------------------------

	if (argc != 2) {
		fprintf(stderr, "usage: %s <port>\n", argv[0]);
		exit(0);
	}
	port = argv[1];

	listenfd = Open_listenfd(port);
	while (1) {
		clientlen = sizeof(clientaddr);
		connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);

		// Determine the domain name and IP address of the client 
		hp = Gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr,
					sizeof(clientaddr.sin_addr.s_addr), AF_INET);
		haddrp = inet_ntoa(clientaddr.sin_addr);
		printf("\n > Server connected to %s (%s)", hp->h_name, haddrp);

		Rio_writen(connfd, buf, strlen(buf));
		Rio_readn((int)connfd, buf, 256);
		printf("\n > %s", buf);

		if ( Fork() == 0 ) {
			printf("\n > Fork");
			Close(listenfd);
			echo(connfd);
			Close(connfd);
			exit(0);
		}

/*
		// Sending size
		strcpy(response, res);
        check = send((int)connfd, response, sizeof(response), 0);
        if (check < 0) {
            fprintf(stderr, "(X) Error al enviar la respuesta: %s\n", strerror(errno));
            printf("\n > Conexion terminada. \n-------------------------- \n");
            Close(connfd);
        }

        printf("\n > Respuesta enviada a %s (%s)", hp->h_name, haddrp);
        printf("\n > Conexion terminada. \n-------------------------- \n");
*/
		echo(connfd);
		Close(connfd);
	}
	exit(0);
}

void echo(int connfd){
	size_t n;
	char buf[MAXLINE];
	rio_t rio;

	Rio_readinitb(&rio, connfd);
	while((n = Rio_readlineb(&rio, buf, MAXLINE)) != 0) {
		printf("server received %lu bytes\n", n);
		Rio_writen(connfd, buf, n);
	}
}
